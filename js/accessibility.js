$(document).ready(function(){
	
	var lang = "Brazilian Portuguese Female";
	//Esconder menu
	$("#menu").click(function(){
		$("#accessibillity").toggleClass("hideMenu","showMenu" );
	})
	
	//Função para inverter contraste
	$(".contraste").click(function(){
		$("body, div, section, header, footer").toggleClass("blackAF", "whiteAF");
		$(".fonteAjustavel").toggleClass("white-textAF");
	})

	//Função para aumentar fonte
	$(".aumentar").click(function(){

		var size = $(".fonteAjustavel").css('font-size');
		size = size.replace('px', '');
		size = parseInt(size) + 2;


		$(".fonteAjustavel").css({'font-size' : size + 'px'});
	});

	//Função para diminuir fonte
	$(".diminuir").click(function(){

		var size = $(".fonteAjustavel").css('font-size');
		size = size.replace('px', '');
		size = parseInt(size) - 2;

		$(".fonteAjustavel").css({'font-size' : size + 'px'});
	});

	//Função para ouvir conteúdo
	$(".ouvir").click(function(){
		
		var x = $(".lerConteudo").text();
		
		if(responsiveVoice.voiceSupport()) {
			responsiveVoice.speak(x, lang, {rate: 1.1});

		}
		else{
			alert("Não suportado");
		}
	});


	//Função para continuar ouvindo conteúdo
	$(".play").click(function(){
		responsiveVoice.resume();
	});

	//Função para pausar conteúdo
	$(".pausa").click(function(){
		responsiveVoice.pause();
	});

	//Toogle menu accessibillity

	
	$("#menu").click(function(){
		$("#accessibility").toggleClass("menuToggleHide","menuToggleShow");

	});
	
});