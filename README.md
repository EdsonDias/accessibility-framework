# Accessibility Framework

Framework para auxiliar a acessibilidade no desenvolvimento de sites.

Para o funcionamento do Accessibility Framework é necessário a importação de algumas depedências.

# Importação do Material Icons
1 - Material Icons: Ícones usados para ajudar a compreensão das funções;
`<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">`

# Importação do jQuery
2 - jQuery: Para facilitar o desenvolvimento em JavaScript;
`<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>`

# Importação da API Responsive Voice
3 - Responsive Voice: API externa que converte texto em áudio.
`<script src='https://code.responsivevoice.org/responsivevoice.js'></script>`


# Funções de acessibilidade
    <span id="accessibility" class="menuToggleShow">
        	<a id="menu" class="tooltipAF" href="#!"><i class="material-icons">accessibility</i><span>Abrir menu de acessibilidade</span></a> 

            <span class="containerAF">
              <span class="contentAF">               
                <a class="contraste white-text centralizarAF tooltipAF" href="#!"><i class="material-icons">brightness_medium</i><span>Alterar contraste</span></a>
                <a class="aumentar white-text centralizarAF tooltipAF" href="#!">A +<span>Aumentar fonte</span></a>
                <a class="diminuir white-text centralizarAF tooltipAF" href="#!">A -<span>Diminuir fonte</span></a>
                <a class="ouvir white-text centralizarAF tooltipAF" href="#!"><i class="material-icons">record_voice_over</i><span>Ouvir conteúdo</span></a>
                <a class="play white-text centralizarAF tooltipAF" href="#!"><i class="material-icons">play_arrow</i><span>Continuar ouvindo</span></a>
                <a class="pausa white-text centralizarAF tooltipAF" href="#!"><i class="material-icons">pause</i><span>Pausar conteúdo</span></a>
              </span>
            </span>
    </span>
    
    # Como usar
    
    Utilize as duas classes (.lerConteudo .fonteAjustavel) para envolver o conteúdo. Ex:
    
    `<div class="lerConteudo fonteAjustavel">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
    </div>`



    
    